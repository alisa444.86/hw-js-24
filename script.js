/**Написать реализацию игры "Сапер".

 Технические требования:

 Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
 Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
 Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю.

 Если в данной ячейке находится мина, игрок проиграл. В таком случае показать все остальные мины на поле.
 Другие действия стают недоступны, можно только начать новую игру.

 Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
 Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все соседние ячейки с цифрами.


 Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
 После первого хода над полем должна появляться кнопка "Начать игру заново",  которая будет обнулять
 предыдущий результат прохождения и заново инициализировать поле.
 Над полем должно показываться количество расставленных флажков, и общее количество мин (например 7 / 10).


 Необязательное задание продвинутой сложности:

 При двойном клике на ячейку с цифрой - если вокруг нее установлено такое же количество флажков, что указано
 на цифре этой ячейки, открывать все соседние ячейки.
 Добавить возможность пользователю самостоятельно указывать размер поля. Количество мин на поле можно считать
 по формуле Количество мин = количество ячеек / 6.*/

const table = document.createElement('table');
const buttonStart = document.createElement('button');
const cells = document.getElementsByTagName('td');
buttonStart.innerText = 'Start New Game';

function createField() {
    for (let i = 0; i < 8; i++) {
        const row = document.createElement('tr');

        for (let i = 0; i < 8; i++) {
            const cell = document.createElement('td');

            cell.classList.add('t-cell');
            row.appendChild(cell);
        }
        table.appendChild(row);
    }
    document.body.appendChild(table);
}

function createMines() {

    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 64);
        if (cells[random].classList.contains('mined')) {
            i -= 1;
            continue;
        } else {
            const mine = document.createElement('div');
            mine.classList.add('mine');
            mine.classList.add('hidden');
            cells[random].classList.add('mined');
            cells[random].appendChild(mine);
        }
    }
    const array = [];
    for (let i = 0; i < 64; i++) {
        array.push(0);
    }
    for (let i = 0; i < cells.length; i++) {

        if (cells[i].classList.contains('mined')) {

            if (cells[i].nextSibling && !cells[i].nextSibling.classList.contains('mined')) {
                cells[i].nextSibling.classList.add('transparent');
                array[i + 1]++;
            }
            if (cells[i].previousSibling && !cells[i].previousSibling.classList.contains('mined')) {
                cells[i].previousSibling.classList.add('transparent');
                array[i - 1]++;
            }
            if (i > 8 && cells[i - 8].nextSibling && !cells[i - 8].nextSibling.classList.contains('mined')) {
                cells[i - 8].nextSibling.classList.add('transparent');
                array[i - 7]++;
            }
            if (i > 8 && cells[i - 8].previousSibling && !cells[i - 8].previousSibling.classList.contains('mined')) {
                cells[i - 8].previousSibling.classList.add('transparent');
                array[i - 9]++;
            }
            if (i < 56 && cells[i + 8].nextSibling && !cells[i + 8].nextSibling.classList.contains('mined')) {
                cells[i + 8].nextSibling.classList.add('transparent');
                array[i + 9]++;
            }
            if (i < 56 && cells[i + 8].previousSibling && !cells[i + 8].previousSibling.classList.contains('mined')) {
                cells[i + 8].previousSibling.classList.add('transparent');
                array[i + 7]++;
            }
            if (i < 56 && !cells[i + 8].classList.contains('mined')) {
                cells[i + 8].classList.add('transparent');
                array[i + 8]++;
            }
            if (i > 8 && !cells[i - 8].classList.contains('mined')) {
                cells[i - 8].classList.add('transparent');
                array[i - 8]++;
            }
        }
    }
    for (let i = 0; i < array.length; i++) {
        if (array[i] !== 0) {
            cells[i].innerText = array[i];
        }
    }
}

createField();
createMines();

table.addEventListener('click', (event) => {
    table.after(buttonStart);
    const mines = document.querySelectorAll('.mine');
    if (event.target.classList.contains('mined')) {
        for (let i = 0; i < cells.length; i++) {
            if (!cells[i].classList.contains('mined'))
                cells[i].classList.add('open');
            if (cells[i].classList.contains('transparent')) {
                cells[i].classList.remove('transparent');
            }
        }
        for (let i = 0; i < mines.length; i++) {
            mines[i].classList.remove('hidden');
        }
    } else if (event.target.classList.contains('transparent')) {
        event.target.classList.remove('transparent');
        event.target.classList.add('open');
    } else {
        event.target.classList.add('open');
        let currentCell = event.target;
        let cellsArray = Array.from(cells);
        let index = cellsArray.indexOf(currentCell);
        let nextCell = currentCell.nextSibling;
        let prevCell = currentCell.previousSibling;
        open();

            if (nextCell) {
                index++;
                open();
                currentCell.classList.add('open');
                currentCell = nextCell;
            }
            if (prevCell && nextCell) {
                index--;
                open();
                currentCell.classList.add('open');
                currentCell = prevCell;
            } else if (prevCell) {
                index-=2;
                open();
                currentCell.classList.add('open');
                currentCell = prevCell;
            }

        function open(i, array) {
            i = index;
            array = cellsArray;

            if (i > 7 && i < array.length && (cells[i - 8].classList.contains('transparent') || !cells[i - 8].classList.contains('mined'))) {
                cells[i - 8].classList.add('open');
                cells[i - 8].classList.remove('transparent');

            }

            if (i < 56 && (cells[i + 8].classList.contains('transparent') || !cells[i + 8].classList.contains('mined'))) {
                cells[i + 8].classList.add('open');
                cells[i + 8].classList.remove('transparent');

            }


        }


    }
});

buttonStart.addEventListener('click', () => {
    location.reload();
});


